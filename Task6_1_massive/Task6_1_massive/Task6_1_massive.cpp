﻿// Task6_1_massive.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

// Даны целые числа а1, а2, а3. Получить целочисленную матрицу [bij]i,j=1,2,3, для
// которой bij = ai - 3aj.

#include "stdafx.h"
#include <iostream>
#include <tchar.h>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	const int n = 3;
	int a[n];
	for (int i = 0; i < n; i++)
	{
		cout << "Enter value a" << i + 1 << ": ";
		cin >> a[i];
		cout << endl;
	}
	int b[n][n];
	int i, j;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			b[i][j] = a[i] - 3 * a[j];
			cout << b[i][j] << " ";
		}
		cout << "\n";
	}

	return 0;
}