﻿
// Найти сумму 1/i!^2, где i [1, 100]



#include <iostream>
#include <stdio.h>
#include <tchar.h>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{

	int n = 0;
	float result = 1;
	int factorial;

	cout << "n:  ";
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		factorial = 1;
		for (int p = 1; p <= i;  factorial *= p, p++);
		result += 1 / pow((float)factorial, 2);   
	}

	cout << "Mult: " << result << endl;


return 0;

}